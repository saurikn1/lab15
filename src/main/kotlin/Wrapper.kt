import dataClass.BoardPost

class Wrapper() {
    fun title(title: String): String {
        return "<p><central><h1>$title</h1></central></p>"
    }
    fun startHTML(path: String): String {
        return "<html>\n" +
                " <head>" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"$path\">" +
                "</head>" +
                "<body>"
    }
    fun endHTML(): String {
        return "</body></html>"
    }


    fun createTable(posts: List<BoardPost>): String {
        var returnString = ""
        for (i in posts) {
            returnString += "<tr><h1>${i.title}</h1></tr>" +
                    "<tr><h4>${i.post}</h4></tr>" +
                    "<tr>Was written by ${i.name}</tr><form action=\"/\" method=\"get\">"
        }
        returnString += "</table>"
        return returnString
    }

    fun createAddForm(first: String, second: String, link: String): String {
        return "<form action=\"/$link\" method=\"get\">\n" +
                "$first <input type=\"text\" name=\"$first\">\n" +
                "$second: <input type=\"password\" name=\"$second\">\n" +
                "<input type=\"submit\" value=\"Submit\" />\n" +
                "</form>"
    }
    fun mainAutorizedLinks(): String {
        return "<form>" +
                "<input type=\"button\" value=\"LogOut\" onclick=\"window.location.href='http://localhost:8080/logout'\" />" +
                "<input type=\"button\" value=\"New Post\" onclick=\"window.location.href='http://localhost:8080/newpost'\" />" +
                "</form>"
    }

    fun mainLinks(): String {
        return "<a href =\"http://localhost:8080/reg\">Registration</a><br>" +
                "<a href =\"http://localhost:8080/log\">Login</a><br>"
    }

    fun mainAutorizedLinks2(): String {
        return "<a href =\"http://localhost:8080/logout\">Logout</a><br>" +
                "<a href =\"http://localhost:8080/newpost\">Add new post</a><br>"
    }
    fun addPost(): String {
        return  "<form action=\"/newpost\" method=\"get\">\n" +
                "<p><center>Title</central></p>" +
                "<p><textarea rows=\"1\" cols=\"45\" name=\"title\"></textarea></p>" +
                "<p><central>Post</central></p>" +
                "<p><textarea rows=\"10\" cols=\"45\" name=\"post\"></textarea></p>" +
                "<input type=\"submit\" value=\"Post!\" />\n"
    }
}