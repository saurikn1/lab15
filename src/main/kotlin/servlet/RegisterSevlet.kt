package servlet

import com.google.gson.Gson
import Wrapper
import dataClass.User
import fileWork.FileWorkUsers
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "register", value = "/reg")
class RegisterServlet : HttpServlet() {
    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val gson = Gson()
        fun write(obj: Any) {
            res.writer.print(obj)
        }
        write(Wrapper().title("To register firstly u need enter login and password:"))
        write(Wrapper().createAddForm("login", "password","reg"))
        if (req.getParameter("login") != null) {
            FileWorkUsers().writeFile(gson.toJson(FileWorkUsers().userAdd(User(req.getParameter("login"),
                    req.getParameter("password")), FileWorkUsers().readFile())))
            res.sendRedirect("/")
        }
    }
}