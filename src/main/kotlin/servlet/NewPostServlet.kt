package servlet

import com.google.gson.Gson
import dataClass.BoardPost
import Wrapper
import fileWork.FileWorkPosts
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "newpost", value = "/newpost")
class NewPostServlet : HttpServlet() {
    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val session = req.getSession(false)
        val gson = Gson()
        fun write(obj: Any) {
            res.writer.print(obj)
        }
        write(Wrapper().title("Add your post here, dear ${req.getSession(true).attributeNames.toList()[0]}"))
        write(Wrapper().addPost())
        if (req.getSession(true) != null) {
            if (req.getParameter("post") != null) {
                FileWorkPosts().writeFile(gson.toJson(FileWorkPosts().postAdd(BoardPost(req.getParameter("title"),
                        req.getParameter("post"), req.getSession(true).attributeNames.toList()[0]), FileWorkPosts().readFile())))
                res.sendRedirect("/")
            }
        }
    }
}