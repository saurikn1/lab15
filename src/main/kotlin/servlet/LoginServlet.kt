package servlet

import com.google.gson.Gson
import dataClass.User
import fileWork.FileWorkUsers
import Wrapper
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "login", value = "/log")
class LoginServlet : HttpServlet() {
    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val gson = Gson()
        fun write(obj: Any) {
            res.writer.print(obj)
        }
        write(Wrapper().title("Enter your email and password here"))
        write(Wrapper().createAddForm("login", "password","log"))
        if (req.getParameter("login") != null) {
            val cur = User(req.getParameter("login"), req.getParameter("password"))
            if (FileWorkUsers().readFile().contains(cur)){
                write("Success")
                val session = req.getSession(true)
                session.setAttribute((cur.login),true)
                res.sendRedirect("/")
            }
            else{
                write("Wrong login/password")
            }
        }
    }
}