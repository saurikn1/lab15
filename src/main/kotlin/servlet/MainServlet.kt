package servlet

import com.google.gson.Gson
import Wrapper
import fileWork.FileWorkPosts
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "servlet.MainServlet", value = "/")
class MainServlet : HttpServlet() {
    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val gson = Gson()
        fun write(obj: Any) {
            res.writer.print(obj)
        }
        write(Wrapper().startHTML("/Users/Saurik/IdeaProjects/lab15/src/main/resources/style.css"))
        write(Wrapper().mainLinks())
        val session = req.getSession(false)
        if (session != null){
            write(Wrapper().mainAutorizedLinks())
        }
        write(Wrapper().createTable(FileWorkPosts().readFile()))
        write(Wrapper().endHTML())
    }
}