package dataClass

data class BoardPost(val title: String, val post: String, val name: String)