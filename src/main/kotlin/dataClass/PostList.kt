package dataClass

class PostList {
    fun addContact(boardPost: BoardPost, boardPostList: List<BoardPost>): List<BoardPost> {
        return boardPostList.plusElement(boardPost)
    }

    fun minusContact(boardPostList: List<BoardPost>, index: Int): List<BoardPost> {
        return boardPostList.minusElement(boardPostList.get(index))
    }
}