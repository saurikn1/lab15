package fileWork

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import java.io.File
import java.io.PrintWriter
import dataClass.User

class FileWorkUsers () {
    fun writeFile(json: String) {
        var writter = PrintWriter(File("/Users/Saurik/IdeaProjects/Lab15/src/main/resources/users.upl"))
        writter.print(json)
        writter.close()
    }

    fun readFile(): List<User> {
        var reader = File("/Users/Saurik/IdeaProjects/Lab15/src/main/resources/users.upl").bufferedReader().readLine()
        return Gson().fromJson <List<User>>(reader)
    }

    fun userAdd(user: User, list: List<User>): List<User> {
        return list.plusElement(user)
    }

    fun userRemove(userList: List<User>, index: Int): List<User> {
        return userList.minusElement(userList.get(index))
    }
}