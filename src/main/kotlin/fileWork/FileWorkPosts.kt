package fileWork

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import dataClass.BoardPost
import java.io.File
import java.io.PrintWriter

class FileWorkPosts () {
    fun writeFile(json: String) {
        var writter = PrintWriter(File("/Users/Saurik/IdeaProjects/Lab15/src/main/resources/base.bd"))
        writter.print(json)
        writter.close()
    }

    fun readFile(): List<BoardPost> {
        var reader = File("/Users/Saurik/IdeaProjects/Lab15/src/main/resources/base.bd").bufferedReader().readLine()
        return Gson().fromJson <List<BoardPost>>(reader)
    }

    fun postAdd(boardPost: BoardPost, list: List<BoardPost>): List<BoardPost> {
        return list.plusElement(boardPost)
    }

    fun postRemove(list: List<BoardPost>, index: Int): List<BoardPost> {
        return list.minusElement(list.get(index))
    }
}